﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPosition : MonoBehaviour
{
    GameObject m_HitObject;
    public GameObject CheckNewPath(GameObject ObjectOfGame)
    {
        RaycastHit2D raycastHit2D;
        raycastHit2D = Physics2D.Raycast(ObjectOfGame.transform.position, Vector2.down);
        if (raycastHit2D != null)
        {
            Debug.Log("hit work");
            m_HitObject = raycastHit2D.collider.gameObject;

            if (m_HitObject.tag == "path")
            {
                return m_HitObject;
            }
            else
            {
                return ObjectOfGame;
            }
        }
        else
        {
            return ObjectOfGame;
        }
    }
}
