﻿using System.Collections;
using UnityEngine;

public class EnemyGeneration : MonoBehaviour
{
    [SerializeField]
    private GameObject[] EnemyPrefeb;

    [SerializeField]
    private GameObject[] EntryPoleOfEnemy;

    int m_TotalEnemyInGamePlay = 4;

    float m_TimeOfEnemyGenration = 10f;

    int EnemyCounter = 0;

    int m_MinEnemy = 1;
    int m_MinEntry = 1;
    int m_MaxEntry;
    int m_MaxEnemy;

    int m_LeftSide = -1;
    int m_RightSide = 1;

    int m_EnemyCounter = 0;

    int m_Side;

    float m_EnemySpeed = 4f;

    private void Start()
    {
        m_Side = m_RightSide;
        m_MaxEnemy = EnemyPrefeb.Length;
        m_MaxEntry = EntryPoleOfEnemy.Length;
        for (int Index = 0; Index < m_TotalEnemyInGamePlay; Index++)
        {
            int l_EntryOfAnimal = 0;
            int l_IndexOfEnemy = Random.Range(m_MinEnemy, m_MaxEnemy);
            EnemyPrefeb[Index] = Instantiate(EnemyPrefeb[l_IndexOfEnemy]);
            EnemyPrefeb[Index].transform.position = EntryPoleOfEnemy[l_EntryOfAnimal].transform.position;
            EnemyPrefeb[Index].SetActive(false);
        }
        StartCoroutine(MovementOfEnemy());
    }


    public IEnumerator MovementOfEnemy()
    {
        yield return new WaitForSeconds(m_TimeOfEnemyGenration);

        int l_id = Random.Range(0, m_TotalEnemyInGamePlay);

        if (!EnemyPrefeb[l_id].activeSelf)
        {
            EnemyPrefeb[l_id].SetActive(true);
        }

        Vector3 position = EnemyPrefeb[l_id].transform.position;
        position.x += (m_EnemySpeed * m_Side);

        EnemyPrefeb[l_id].transform.position = position;
        yield return new WaitForSeconds(4);

        StartCoroutine(Movement());

        StartCoroutine(MovementOfEnemy());
    }

    public IEnumerator Movement()
    {
        yield return new WaitForSeconds(1);


        if (m_Side == m_RightSide)
        {
            m_Side = m_LeftSide;
        }
        else
        {
            m_Side = m_RightSide;
        }
        StartCoroutine(Movement());

    }
}
