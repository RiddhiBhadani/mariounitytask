﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{


    float m_MarioSpeed = 0.9f;
    float m_MarioJump = 0.04f;

    public Sprite m_MarioLeftSide;
    public Sprite m_MarioRightSide;

    int m_leftSide = -1;

    public SpriteRenderer m_MarioSprite;

    public Rigidbody2D m_MarioRigidBody;

    //public GameObject m_Mario;

    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            m_MarioSprite.sprite = m_MarioLeftSide;
            m_MarioRigidBody.velocity += (Vector2.left);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            m_MarioSprite.sprite = m_MarioRightSide;
            m_MarioRigidBody.velocity += (Vector2.right);
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            m_MarioRigidBody.velocity += Vector2.up * 5f;
        }    
    }
}
